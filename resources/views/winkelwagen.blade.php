@extends('layout')
@section('title', 'Winkelwagen')

@section('content')

<section class="h-100 h-custom" style="background-color: #d2c9ff;">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12">
        <div class="card card-registration card-registration-2" style="border-radius: 15px;">
          <div class="card-body p-0">
            <div class="row g-0">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="d-flex justify-content-between align-items-center mb-5">
                    <h1 class="fw-bold mb-0 text-black">Winkelwagen</h1>
                    <h6 class="mb-0 text-muted"></h6>

                  </div>
                  <div class="d-flex justify-content-between align-items-center mb-5">
                    <h5 class="fw-bold mb-0 text-black">Product Naam</h5>
                    <h6 class="mb-0 ">Aantal</h6>
                    <h6 class="mb-0 ">Prijs</h6>
                    <h6 class="mb-0 ">Subtotaal</h6>

                    <h6 class="mb-0 ">Verwijderen</h6>



                  </div>
                  <hr class="my-4">
                  @php
                        $totalPrice = 0; // Initialize the total price
                        $discount = 0.15; // 15% discount

                  @endphp

                  @guest
        

                    @if(session('cart'))
                     @foreach(session('cart') as $product_id => $details)
                          <div class="d-flex justify-content  mb-5">
                              <div class="col-md-2 col-lg-3 col-xl-3">
                                  <h6 class="text-muted">{{ $details['Pname'] }}</h6>
                              </div>
                              @if($details['available'] == TRUE)

                              <div class="col-md-3 col-lg-3 col-xl-2">
                                 <div class="input-group">
                                 


                                  <form action="{{ route('update-cart-product', ['product_id' => $product_id]) }}" method="post">
                                      {{ csrf_field() }}
                                      <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="Pstock">
                                          <i class="fas fa-minus"></i>
                                      </button>
                                     </span>
                                      <input type="number" name="Pstock" class="form-control input-number" value="{{ $details['Pstock'] }}" min="1">
                                      <span class="input-group-btn">
                                          <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="Pstock">
                                              <i class="fas fa-plus"></i>
                                          </button>
                                      </span>
                                      <button type="submit" class="btn btn-primary">winkelwagen bijwerken</button>
                                  </form>
                                 </div>
                                  @php
                                       $totalPrice += $details['Pprice'] * $details['Pstock']; // Update the total price
                                   @endphp
                                  </div>
                                  <div class="col-md-2 col-lg-3 col-xl-2 offset-lg-2">
                                      <h6 class="mb-0">€{{ $details['Pprice'] }}</h6>
                                      <input type="hidden" value="{{ $details['Pprice'] }}">
                                  </div>
                                  <div class="col-md-3 col-lg-2 col-xl-2">
                                      <h6 class="mb-0">€ <span id="total-price-{{ $product_id }}">{{ $details['Pprice'] * $details['Pstock'] }}</span></h6>
                                  </div>
                              @else
                              <div class="col-md-5 col-lg-5 col-xl-5">
                                    <h6 class="mb-0"> Deze product is niet beschikbaar</h6>
                              </div>
                              @endif
                              <div class="col-md-3 col-lg-2 col-xl-2 text-end">
                                  <form action="{{ route('delete-cart-product', ['product_id' => $product_id]) }}" method="post">
                                      {{ method_field('DELETE') }}
                                      {{ csrf_field() }}
                                      <button type="submit" class="btn btn-outline-danger"> X</button>
                                  </form>
                              </div>
                          </div>
                      @endforeach
                    @else
                    <div class="col-md-5 col-lg-5 col-xl-5">
                        <h6 class="mb-0"> Winkelwagen is leeg</h6>
                      </div>

                    @endif

                @else
                  @if(count($cart) > 0)

                    @foreach($cart as $shoppingcart)
                    <div class="d-flex justify-content  mb-5">
                        <div class="col-md-2 col-lg-3 col-xl-3  ">
                                <h6 class="text-muted">{{ $shoppingcart->productcart->Pname }}</h6>
                        </div>
                        @if($shoppingcart->productcart->available == TRUE)

                        <div class="col-md-5 col-lg-3 col-xl-2">

                        <div class="input-group">

                        <span class="input-group-btn">
                            <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="Pstock">
                                <i class="fas fa-minus"></i>
                            </button>
                        </span>
                        <form action="{{ url('/updatecartitem' . '/' . $shoppingcart->cart_id) }}" method="post">
                            {{ csrf_field() }}
                            <input type="number" name="Pstock" class="form-control input-number" value="{{ $shoppingcart->amount }}" min="1">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="Pstock">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </span>
                            <button type="submit" class="btn btn-primary">winkelwagen bijwerken</button>
                        </form>
                        </div>
                        @php
                              $totalPrice += $shoppingcart->productcart->Pprice * $shoppingcart->amount;
                        @endphp
                        </div>
          
                        <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-1 ">
                          <h6 class="text-muted">{{ $shoppingcart->productcart->Pprice }}</h6>

                        </div>
                        <div class="col-md-3 col-lg-2 col-xl-2">
                              <h6 class="mb-0"><span id="total-price-{{  $shoppingcart->cart_id }}">€ {{ $shoppingcart->productcart->Pprice *  $shoppingcart->amount }}</span></h6>
                        </div>
                        @else
                        <div class="col-md-5 col-lg-5 col-xl-5">
                              <h6 class="mb-0"> Deze product is niet beschikbaar</h6>
                        </div>
                        @endif

                        <div class="col-md-3 col-lg-2 col-xl-2 text-end">
                            <form action="{{ url('/deletecartproduct' . '/' . $shoppingcart->cart_id) }}" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-outline-danger"> X</button>
                            </form>
                        </div>

                    </div>
                @endforeach
              @else
                 <div class="col-md-5 col-lg-5 col-xl-5">
                   <h6 class="mb-0"> Winkelwagen is leeg</h6>
                 </div>

              @endif

          @endguest

                  <hr class="my-4">

                  <div class="pt-5">
                    <h6 class="mb-0"><a href="suplementen" class="text-body"><i
                          class="fas fa-long-arrow-alt-left me-2"></i>Terug naar winkel</a></h6>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 bg-grey">
                <div class="p-5">
                  <hr class="my-4">
                  @guest

                        <div class="d-flex justify-content-between mb-4">
                          <h5 class="text-uppercase">Totale prijs</h5>
                          <h5>€ {{ number_format($totalPrice, 2) }}</h5>
                        </div>

        
                        <!-- <h5 class="text-uppercase mb-3">Kortingscode</h5>
                        <div class="mb-5">
                            <div class="form-outline">
                                <input type="text" id="discount-code" class="form-control form-control-lg" />
                                <label class="form-label" for="discount-code">Type jouw code in</label>
                            </div>
                            <button id="apply-discount" class="btn btn-success">Apply Discount</button>
                        </div> -->

 

                      <!-- Display the discounted total price -->
                      @if (count(session('cart')) > 0)
                          <a href="bestelbezoeker" class="btn btn-dark btn-block btn-lg" data-mdb-ripple-color="dark">Bestellen</a>
                      @else
                          <button class="btn btn-dark btn-block btn-lg" disabled>Bestellen</button>
                      @endif
                @else
                        <div class="d-flex justify-content-between mb-4">
                            <h5 class="text-uppercase">Alle producten</h5>
                            <h5>€ {{ ( $totalPrice ) }}</h5>
                          </div>

          
                        @if(count($cart) > 0)
                        <a href="bestellen"  class="btn btn-dark btn-block btn-lg" data-mdb-ripple-color="dark">Bestellen</a>
                        @else
                        <button class="btn btn-dark btn-block btn-lg" disabled>Bestellen</button>

                        @endif
                        
                  @endguest


                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



@endsection


@section('scripst')
<script>
    // Wait for the DOM to be ready
    $(document).ready(function () {

            $('.btn-number').click(function (e) {
                e.preventDefault();

                var fieldName = $(this).data('field');
                var type = $(this).data('type');
                var input = $(this).closest('.input-group').find("input[name='" + fieldName + "']");
                var product_id = input.data('product-id');
                var price = parseFloat(input.closest('.input-group').find("input[type='hidden']").val());
                var currentVal = parseInt(input.val(), 10);

                if (!isNaN(currentVal)) {
                    if (type == 'minus') {
                        if (currentVal > 1) {
                            input.val(currentVal - 1).change();
                        }
                    } else if (type == 'plus') {
                        input.val(currentVal + 1).change();
                    }

                    // Update the total price
                    var total = price * parseInt(input.val());
                    $('#total-price-' + product_id).text('€ ' + total);
                } else {
                    input.val(1);
                }
            });
      });

    


    
    
</script>


 @endsection
