<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ url('css/style.css')}}">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
 
    </head>
    <body class="antialiased">
         <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
            <a class="navbar-brand" href="#">
                <p>AliNutrition</p>
            </a>
              <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">

                        <li class="nav-item ">
                            <a class="nav-link " href="categorie">Categorieen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="producten">Suplementen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="bestellingenoverzicht">bestelingen</a>
                        </li>

                        <li class="nav-item">
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="nav-link btn-danger" href="logout">logout</button>
                                </form>

                        </li>
                </ul>
               </div>
           </div>
         </nav>
        <div>
            @yield('content')
        </div>
        @yield('scripst')


    </body>
</html>
