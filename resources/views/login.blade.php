
@extends('layout')
@section('title', 'Login')
@section('content')
<div class="row justify-content-center mt-5">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title text-center">login</h1>
                    </div>
                    <div class="card-body">
                           @if(Session::has('error'))
                                <div class="alert alert-danger">
                                    {{Session::get('error')}}
                                </div>
                                @endif
                        <form action="{{ route('login')}}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label class="form-label">Emailadres</label>
                                <input type="email" name="email" required class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Wachtwoord</label>
                                <input type="password" name="password" required class="form-control">
                            </div>
                            <div class="mb-3">
                            <p class="text-center">Nog geen klant? <a href="registreren">Maak een account aan</a></p>
                                <div class="d-grid">
                                    <button class="btn btn-primary">LOGIN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
 @endsection


