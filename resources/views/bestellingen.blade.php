@extends('layout')
@section('title', 'Bestellingen')

@section('content')
<section class="h-100 gradient-custom">
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-lg-10 col-xl-8">
        <div class="card" style="border-radius: 10px;">
          <div class="card-header px-4 py-5">
            <h5 class="text-muted mb-0">Bekijk al jouw bestellingen!</h5>
          </div>
          @php
              $totalPrice = 0; 

          @endphp
          <div class="card-body p-4">
          @if(count($orders) > 0)

            <div class="d-flex justify-content-between align-items-center mb-4">
              <p class="lead fw-normal mb-0" style="color: #a8729a;">Alle bestellingen</p>
              <!-- <p class="small text-muted mb-0">Receipt Voucher : 1KAU9-84UIL</p> -->
            </div>
            @foreach($orders as $order)
    @foreach($orderitems as $orderItem)
        @if ($order->order_id == $orderItem->order_id)
            <div class="card shadow-0 border mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="products/{{$orderItem->orderproduct->Pimage}}"
                                class="img-fluid" alt="Phone">
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                            <p class="text-muted mb-0">{{ $orderItem->orderproduct->Pname }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                            <p class="text-muted mb-0 small">Smaak: {{ $orderItem->orderproduct->Ptaste }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                            <p class="text-muted mb-0 small">Gewicht: {{ $orderItem->orderproduct->Pweight }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                            <p class="text-muted mb-0 small">Aantal: {{ $orderItem->Pamount }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                            <p class="text-muted mb-0 small">Prijs: {{ $orderItem->Pamount * $orderItem->price }}</p>
                        </div>
                        <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                            <p class="text-muted mb-0 small">Datum: {{ $order->created_at }}</p>
                        </div>
                    </div>
                    <hr class="mb-4" style="background-color: #e0e0e0; opacity: 1;">
                </div>
                @php
                    $totalPrice += $orderItem->price * $orderItem->Pamount;
                @endphp
            </div>
        @endif
    @endforeach
@endforeach

            <!-- einde -->
            @else
            <div class="col-md-6 text-center d-flex justify-content-center align-items-center">
                    <p class="text-muted mb-1">Er zijn nog geen bestellingen geplaatst</p>
            </div>
            @endif
 


          </div>
          <div class="card-footer border-0 px-4 py-5"
            style="background-color: #a8729a; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
            <h5 class="d-flex align-items-center justify-content-end text-white text-uppercase mb-0">Totaal 
              Betaald:   <span class="h2 mb-0 ms-2"> € {{ $totalPrice }}</span></h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection