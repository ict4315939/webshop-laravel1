@extends('layoutadmin')
@section('title', 'Product Toevoegen')

@section('content')


<div class="max-w-7xl mx-auto p-6 lg:p-8">
   <div class="flex justify-center mt-16 px-0 sm:items-center sm:justify-between">

     <div class="ml-4 text-center text-sm text-gray-500 dark:text-gray-400 sm:text-right sm:ml-0">
        <div class="row justify-content-center mt-5">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title text-center">create a new product</h1>
                    </div>
                        <div class="card-body">
                            <form action="{{ url('producten') }}" method="post" enctype="multipart/form-data">
                              {!! csrf_field() !!}

                                <div class="mb-3">
                                    <label class="form-label">Naam van product</label>
                                    <input type="text" name="Pname" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Smaak</label>
                                    <input type="text" name="Ptaste"  class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Beschrijving</label>
                                    <textarea  name="Pdescription"  class="form-control"></textarea>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Aantal</label>
                                    <input type="text" name="Pstock"  class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Prijs</label>
                                    <input type="text" name="Pprice"  class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Gewicht</label>
                                    <input type="text" name="Pweight"  class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Afbeelding</label>
                                    <input type="file" name="Pimage"  class="form-control">
                                </div>
                                <div class="mb-3" class="form-control">
                                <label class="form-label">Categorie</label>

                                <select class="form-control" name="category_id">
                                     @foreach ($categorie as $category)
                                           <option value="{{ $category->id }}">{{ $category->category }}</option>
                                     @endforeach
                               </select>
                                </div>
                                <div class="mb-3">
                                    <div class="d-grid">
                                        <button class="btn btn-primary">Create</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>   
 
        </div>
     </div>
   </div>
 </div>
@endsection