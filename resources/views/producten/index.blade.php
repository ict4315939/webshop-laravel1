@extends('layoutadmin')
@section('title', 'Producten')


@section('content')
 
        <div class="ml-4 text-center text-sm text-gray-500 dark:text-gray-400 sm:text-right sm:ml-5">
            <h2>Producten</h2><br>
            <a href="{{ url('/producten/create') }}" class="toevoeg">Voeg een product toe</a><br>

        </div>




 <div class="card mx-auto" style="width: 70rem;">
   @if(session('message'))
        <div class="text-center">
            <p class="alert alert-secondary">{{ session('message') }}</p>
        </div>
      @endif
    <table class="table table-bordered table-responsive-md">

       <thead>
       @if($producten->isEmpty())
        
          <p class="alert ">Geen Producten gevonden</p>

       @else
        <tr>
        <th scope="col">Product naam</th>
        <th scope="col">Smaak</th>
        <th scope="col">Beschrijving</th>
        <th scope="col">Aantal</th>
        <th scope="col">Prijs</th>
        <th scope="col">Gewicht</th>
        <th scope="col">Afbeelding</th>
        <th scope="col">Categorie</th>
        <th scope="col">Verwijderen</th>
        <th scope="col">Aanpassen</th>
        </tr>
        </thead>
        <tbody>

          @foreach($producten as $product)
              <tr>
              <td>{{ $product->Pname }}</td>
              <td>{{ $product->Ptaste }}</td>
              <td>{{ $product->Pdescription }}</td>
              <td>{{ $product->Pstock }}</td>
              <td>{{ $product->Pprice }}</td>
              <td>{{ $product->Pweight }}</td>
              <td> 
                <img src="products/{{ $product->Pimage }}" width="100">
              </td>
              <td>{{ $product->category->category }}</td>
              <td>            
                  <form method="POST" action="{{ url('/updateproductitem' . '/' . $product->product_id) }}" accept-charset="UTF-8" style="display:inline">
                  {!! csrf_field() !!}
                   <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Weet je zeker dat je dit product wilt verwijderen?')" title="Delete categorie"> Delete</button>
                  </form></td>
                  <td>
                  <a href="{{ url('/producten/' . $product->product_id . '/edit') }}" title="Edit product"><button class="btn btn-primary btn-sm"> Edit</button></a>
                  </td>
              </tr>
            @endforeach
       @endif
      </tbody>
    </table>
 </div>


@endsection
