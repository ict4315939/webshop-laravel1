@extends('layout')
@section('title', 'Bestellen')
@section('content')

<h2>Plaats je besteling</h2><br>

<div class="container d-flex justify-content-center mt-2 mb-2">



    <div class="row mt-3 mb-3">
  
    <div class="col-md-7">  
          <form method="post" action="{{ route('ordercart')}}">
            @csrf


                
                <span>Vul je gegevens in</span>

                    
                    <div class="card">
                    @foreach($user as $userinfo)


                      <div class="card-header p-3" id="headingTwo">
                           <span>Voornaam</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" class="form-control" name="voornaam" value="{{$userinfo->voornaam}}" required>
                            </div>
                            <span>Achternaam</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" class="form-control" name="achternaam" value="{{$userinfo->achternaam}}" required>
                            </div>
                            <span>Stad</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" class="form-control"  name="woonplaats" value="{{$userinfo->woonplaats}}" required>
                            </div>
                            <span>Straatnaam</span>
                            <div class="d-flex align-items-center justify-content-between" >

                            <input type="text" class="form-control"  name="straat" value="{{$userinfo->straat}}" required>
                            </div>
                            <span>Huisnummer</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" class="form-control"  name="huisnummer" value="{{$userinfo->huisnummer}}" required>
                            </div>
                            <span>Postcode</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" class="form-control"  name="postcode" value="{{$userinfo->postcode}}" required>
                            </div>
                            <span>Email</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" class="form-control"  name="email" value="{{$userinfo->email}}" required>
                            </div>

                      </div>
                      @endforeach
          
                    </div>
              </div>

              <div class="col-md-5">
                  <span>Betalingen</span>

                  <div class="card">
                    @php

                      $totalPrice = 0;
                      $discount = 0.15;

                    @endphp
                    @foreach($cartinfo as $product)

                    <div class="d-flex justify-content-between p-3">

                      <div class="d-block flex-column">
                       <input type="hidden" name="product_id" value="{{ $product->product_id }}">
                        <input type="hidden" name="Pamount" value="{{ $product->amount }}">
                        <input type="hidden" name="price" value="{{ $product->productcart->Pprice }}">


                      <span>{{ $product->productcart->Pname }} </span>
                        <span class="billing">Totale Prijs</span>
                        <span class="billing">€{{ $product->productcart->Pprice * $product->amount }} </span>
                        
                      </div>

                      <div class="mt-1">
                        <sup class="super-price">Aantal: {{ $product->amount }}</sup>
                        <span class="super-price">Perstuk: €{{ $product->productcart->Pprice }}</span>
                      </div>
                      @php
                        $totalPrice += $product->productcart->Pprice * $product->amount + 2.50;
                      @endphp
                      
                    </div>
                    @endforeach

                    <hr class="mt-0 line">
                    <div class="p-2">
                    <div class="d-flex justify-content-between mb-3">
                             <span>Kortingscode:</span><br>
                              <input type="text" id="discount-code" class="form-control" />
                          </div>
                          <button type="button" id="apply-discount" class="btn btn-success">Kortingscode Activeren</button>
                      </div>

                    </div>


                    <div class="p-3">

                      <div class="d-flex justify-content-between mb-2">

                        <span>Bezorgkosten</span>
                        <span>€2.50</span>
                        
                      </div>
                    </div>

                    <hr class="mt-0 line">

                    <div class="p-3 d-flex justify-content-between">

                      <div class="d-flex flex-column">

                        <span>Totale prijs</span>

                      </div>
                      <span id="discounted-price">€ {{ $totalPrice }}</span>
                      <input type="hidden" name="totalprice" value="{{ $totalPrice }}">

                    </div>

                    <div class="p-3">

                        <button type="submit" class="btn btn-primary btn-block free-button">Bestel</button> 
                    </div>                    
                  </div>
              </div>
       </form>

              
 </div>
            
</div>

@endsection
@section('scripst')
<script>

    
    $(document).ready(function () {
    // Get the initial total price
    var initialTotalPrice = {{ $totalPrice }};
    
    // Get the discount amount
    var discount = {{ $discount }};
    
    // Apply discount when the "Apply Discount" button is clicked
    $("#apply-discount").click(function () {
        var discountCode = $("#discount-code").val();
        
        if (discountCode === "alialfadli") {
            // Calculate the discounted price
            var discountedPrice = initialTotalPrice - (initialTotalPrice * discount);
            $("#discounted-price").text('€ ' + discountedPrice.toFixed(2));
        } else {
            // If the discount code is incorrect, display the regular total price
            $("#discounted-price").text('€ ' + initialTotalPrice.toFixed(2));
        }
    });
});

    
    
</script>


 @endsection
