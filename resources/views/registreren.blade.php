@extends('layout')
@section('title', 'registreren')

@section('content')
        <div class="row justify-content-center mt-5">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title text-center">Registreren</h1>
                    </div>
                    <div class="card-body">
                        @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                        @endif

                        <form action="{{ route('registreren')}}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label class="form-label">Voornaam</label>
                                <input type="text" name="voornaam" required class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Achternaam</label>
                                <input type="text" name="achternaam" required class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Woonplaats</label>
                                <input type="text" name="woonplaats" required  class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Straat</label>
                                <input type="text" name="straat" required class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Huisnummer</label>
                                <input type="text" name="huisnummer" required class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Postcode</label>
                                <input type="text" name="postcode" required class="form-control">
                            </div>
                            <input type="hidden" name="role_id"  value="1" class="form-control">


                            <div class="mb-3">
                                <label class="form-label">Email</label>
                                <input type="email" name="email" required class="form-control">
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Wachtwoord</label>
                                <input type="password" name="password" required class="form-control">
                            </div>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                             <div class="mb-3">
                               <p class="text-center">Ben je al een klant? <a href="login">Inloggen</a></p>
                                <div class="d-grid">
                                    <button class="btn btn-primary">Registreren</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
@endsection

