@extends('layoutadmin')
@section('title', 'Bestellingen overzicht')

@section('content')
<div class="ml-4 text-center text-sm text-gray-500 dark:text-gray-400 sm:text-right sm:ml-5">
    <h2>Bestellingen overzicht</h2><br>
</div>
<div class="container">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                @if(count($orderinfo) > 0)
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Voornaam</th>
                            <th>Achternaam</th>
                            <th>Woonplaats</th>
                            <th>Straat</th>
                            <th>Huisnummer</th>
                            <th>Postcode</th>
                            <th>Email</th>
                            <th>Betaald?</th>
                            <th>Totaleprijs</th>
                            <th>Datum</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orderinfo as $orders)
                        <tr>
                            <td>
                                <button class="btn btn-default btn-xs btn-collapse btn-primary" data-toggle="collapse" data-target="#demo{{ $orders->order_id }}">
                                    <span class="glyphicon glyphicon-eye-open "></span> Bekijk bestelling
                                </button>
                            </td>
                            <td>{{ $orders->voornaam }}</td>
                            <td>{{ $orders->achternaam }}</td>
                            <td>{{ $orders->woonplaats }}</td>
                            <td>{{ $orders->straat }}</td>
                            <td>{{ $orders->huisnummer }}</td>
                            <td>{{ $orders->postcode }}</td>
                            <td>{{ $orders->email }}</td>
                            @if($orders->paid == true)
                            <td>Betaald!</td>
                            @else
                            <td>Nog niet Betaald!</td>
                            @endif
                            <td>{{ $orders->totalprice }}</td>
                            <td>{{ $orders->created_at }}</td>
                        </tr>
                        <tr class="hiddenRow">
                            <td colspan="6">
                                <div class="accordian-body collapse" id="demo{{ $orders->order_id }}">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr class="info">
                                                <th>Image</th>
                                                <th>Productnaam:</th>
                                                <th>Smaak:</th>
                                                <th>Gewicht:</th>
                                                <th>Aantal:</th>
                                                <th>Prijs:</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orderitems as $orderItem)
                                             @if ($orders->order_id == $orderItem->order_id)
                                             <tr>



                                                <td>
                                                    <img src="products/{{$orderItem->orderproduct->Pimage}}" width="100">
                                                </td>
                                                <td>{{ $orderItem->orderproduct->Pname }}</td>
                                                <td>{{ $orderItem->orderproduct->Ptaste }}</td>
                                                <td>{{ $orderItem->orderproduct->Pweight }}</td>
                                                <td>{{ $orderItem->Pamount }}</td>
                                                <td>
                                                    {{ $orderItem->Pamount * $orderItem->price }}
                                                </td>
                                                </tr>

                                                @endif
                                             @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <p class="alert ">Geen Bestellingen  gevonden</p>

                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
    // Add a click event handler to open and close the collapse
    $(".btn-collapse").click(function() {
        var $row = $(this).closest('tr');
        var $collapse = $row.next('.hiddenRow');
        
        if ($collapse.hasClass('in')) {
            $collapse.collapse('hide');
        } else {
            $collapse.collapse('show');
        }
    });
});
</script>
@endsection
