@extends('layout')
@section('title', 'bestellen')
@section('content')

<h2>Plaats je besteling</h2><br>




<div class="container d-flex justify-content-center mt-2 mb-2">



    <div class="row mt-3 mb-3">

              <div class="col-md-7">  
              <form method="post" action="{{ route('bestellen')}}">
               @csrf



                
                <span>Vul je gegevens in</span>

                    
                    <div class="card">


                      <div class="card-header p-3" id="headingTwo">
                           <span>Voornaam</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="voornaam" required class="form-control">
                            </div>
                            <span>Achternaam</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="achternaam" required class="form-control">
                            </div>
                            <span>Stad</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="woonplaats" required class="form-control">
                            </div>
                            <span>Straatnaam</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="straat" required class="form-control">
                            </div>
                            <span>Huisnummer</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="huisnummer" required class="form-control">
                            </div>
                            <span>Postcode</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="postcode" required class="form-control">
                            </div>
                            <span>Email</span>
                            <div class="d-flex align-items-center justify-content-between">

                            <input type="text" name="email" required class="form-control">
                            </div>

                      </div>
          
                    </div>
              </div>

              <div class="col-md-5">
                  <span>Betalingen</span>

                  <div class="card">
                  @php
                        $totalPrice = 0;
                        $discount = 0.15;


                  @endphp
                  @foreach(session('cart') as $product_id => $details)


                    <div class="d-flex justify-content-between p-3">

                      <div class="d-block flex-column">
                        <input type="hidden" name="product_id" value="{{ $product_id }}">
                        <input type="hidden" name="Pamount" value="{{ $details['Pstock'] }}">
                        <input type="hidden" name="price" value="{{ $details['Pprice'] }}">



                        <span>{{ $details['Pname'] }} </span>
                        <span class="billing">Totale Prijs</span>
                        <span class="billing">€{{ $details['Pprice'] * $details['Pstock'] }} </span>

                        
                      </div>

                      <div class="mt-1">
                        <sup class="super-price">Aantal: {{ $details['Pstock'] }}</sup>
                        <span class="super-price">Perstuk: €{{ $details['Pprice'] }}</span>
                      </div>
                      @php
                         $totalPrice += $details['Pprice'] * $details['Pstock'] + 2.50; 

                     @endphp
                      
                    </div>
                    @endforeach


                    <hr class="mt-0 line">
                    <div class="p-2">

                     <div class="d-flex justify-content-between mb-3">
                             <span>Kortingscode:</span><br>
                              <input type="text" id="discount-code" class="form-control" />
                          </div>
                          <button type="button" id="apply-discount" class="btn btn-success">Kortingscode Activeren</button>
                      </div>

                    <div class="p-3">

                      <div class="d-flex justify-content-between mb-2">

                        <span>Bezorgkosten</span>
                        <span>€2.50</span>
                        
                      </div>
                    </div>

                    <hr class="mt-0 line">

                    <div class="p-3 d-flex justify-content-between">

                      <div class="d-flex flex-column">

                        <span>Totale prijs</span>

                      </div>
                      <span id="discounted-price">€ {{ $totalPrice }}</span>
                      <input type="hidden" name="totalprice" value="{{ $totalPrice }}">
                    </div>

                    <div class="p-3">
                        <button type="submit" class="btn btn-primary btn-block free-button">Bestel</button> 
                    </div>                    
                  </div>
                  

              </div>
             </form>

 
              
 </div>
            
</div>

@endsection
@section('scripst')
<script>

    
    $(document).ready(function () {
    // Get the initial total price
    var initialTotalPrice = {{ $totalPrice }};
    
    // Get the discount amount
    var discount = {{ $discount }};
    
    // Apply discount when the "Apply Discount" button is clicked
    $("#apply-discount").click(function () {
        var discountCode = $("#discount-code").val();
        
        if (discountCode === "alialfadli") {
            // Calculate the discounted price
            var discountedPrice = initialTotalPrice - (initialTotalPrice * discount);
            $("#discounted-price").text('€ ' + discountedPrice.toFixed(2));
        } else {
            // If the discount code is incorrect, display the regular total price
            $("#discounted-price").text('€ ' + initialTotalPrice.toFixed(2));
        }
    });
});

    
    
</script>


 @endsection