
@extends('layout')
@section('title', 'Suplementen')

@section('content')

<h2>Onze producten</h2><br>

<section style="background-color: #eee;">

    <div class="container py-5">
      <form action="/filter" method="get">
      <label> categorieën</label>
         <select class="form-select" name="category">
         <option>AlleProducten</option>

                @foreach ($categorie as $category)

                      <option value="{{ $category->id }}">{{ $category->category }}</option>
                @endforeach
         </select>
         <br>
         <button type="submit" class="btn btn-primary">Filter</button>
         <br>
      </form>
      <br>

      @if(session('success'))
       <div class="alert alert-primary">
        {{ session('success')}}

       </div>
      @endif

      <div class="row">
      @if(count($producten) > 0)

        @foreach($producten as $product)

        <div class="col-md-3 mb-2">
          <div class="card">
            <div class="d-flex justify-content-between p-2">
              <p class="lead mb-0">{{$product->Pname}}</p>
            </div>
            <img src="products/{{$product->Pimage}}" class="card-img" />
            <div class="card-body">
              <div class="d-flex justify-content-between">
                <p class="small">{{$product->Pdescription}}</p>
              </div>
              <div class="d-flex justify-content-between mb-3">
                <h5 class="mb-0">Smaak: {{$product->Ptaste}}  </h5>
                <h5 class="text-dark mb-0"> Prijs: {{$product->Pprice}}</h5>
              </div>`
              @guest

                 <a href="{{ route('addtocart', $product->product_id)}}" class="btn btn-danger">Add To Cart</a>
              @else
              <a href="{{ route('add_to_cart', $product->product_id)}}" class="btn btn-primary">Add To Cart</a>


              @endguest
     
            </div>
          </div>
          </div>
          @endforeach


          @else
          <p class="lead mb-0">Op dit moment zijn er geen producten beschikbaar</p>

          @endif
      </div>
    </div>
 </section>

@endsection

