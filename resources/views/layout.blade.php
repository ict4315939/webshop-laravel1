<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-...." crossorigin="anonymous"/>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 
        <link rel="stylesheet" href="{{ url('css/style.css')}}">
    </head>
    <body>


         <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
            <a class="navbar-brand" href="#">
                <p>AliNutrition</p>
            </a>
              <div class="collapse navbar-collapse">
              <ul class="navbar-nav ml-auto">
                    @guest
                        <li class="nav-item ">
                            <a class="nav-link " href="/welcome">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="suplementen">Suplementen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="winkelwagen"><i class="fa fa-shopping-cart fa-1x">Winkelwagen</i><span class="badge bg-danger">{{ count((array) session('cart')) }}</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="login">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="registreren">Registreren</a>
                        </li>

                    @else
                        <li class="nav-item ">
                            <a class="nav-link " href="/welcome">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="suplementen">Suplementen</a>
                        </li>
                        @php
                        $userCartCount = Auth::user()->cartItems->count();
                        @endphp
                        <li class="nav-item">
                        <a class="nav-link " href="winkelwagen"><i class="fa fa-shopping-cart fa-1x">Winkelwagen</i><span class="badge bg-danger">{{$userCartCount}}</span></a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="bestellingen">bestellingen</a>
                        </li>
                        <li class="nav-item">
                            @auth
                                <form method="POST" action="{{ route('logou') }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="nav-link btn-danger" href="logout">logout</button>
                                </form>
                            @endauth
                        </li>
                    @endguest
                </ul>
               </div>
           </div>
         </nav>
        <div>
            @yield('content')
        </div>


        @yield('scripst')



    </body>
</html>
