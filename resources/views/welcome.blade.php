@extends('layout')
@section('title', 'Home')
@section('content')
    <div  class="slide">

            <div class="inner">
              <div class="active">
              <img src="{{ URL ('images/foto.jpg')}}" class="d-flex w-100" alt="..." style="height: 600px;">
              </div>
         </div>

            <div class="max-w-7xl mx-auto p-6 lg:p-8">
                <div class="flex justify-center mt-16 px-0 sm:items-center sm:justify-between">

                    <div class="ml-4 text-center text-sm text-gray-500 dark:text-gray-400 sm:text-right sm:ml-0">
                        <h2>Welkom</h2><br>
                        <P>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni quod suscipit incidunt id nulla doloribus provident neque perspiciatis sapiente numquam recusandae reiciendis, soluta quae impedit, beatae assumenda cupiditate quidem. Voluptas mollitia, hic ab obcaecati saepe voluptates suscipit voluptatibus culpa assumenda modi deserunt nisi molestiae doloribus odio dicta vitae labore eius!</P>
						<h2>Onze Producten</h2><br>

						<a class="btn btn-success" href="suplementen">Onze Producten</a>
                    </div>
                </div>
            </div>
	<div class="container my-5">
  <!-- Footer -->
		<footer class="text-center text-white" style="background-color: #3f51b5">
			<!-- Grid container -->
			<div class="container">
			<!-- Section: Links -->
					<section class="mt-5">
						<!-- Grid row-->
						<div class="row text-center d-flex justify-content-center pt-5">
						<!-- Grid column -->
						<div class="col-md-2">
							<h6 class="text-uppercase font-weight-bold">
							<a href="welcome" class="text-white">Over ons</a>
							</h6>
						</div>

						<div class="col-md-2">
							<h6 class="text-uppercase font-weight-bold">
							<a href="suplementen" class="text-white">Producten</a>
							</h6>
						</div>
						<div class="col-md-2">
							<h6 class="text-uppercase font-weight-bold">
							<a href="registreren" class="text-white">Registreren</a>
							</h6>
						</div>

						<div class="col-md-2">
							<h6 class="text-uppercase font-weight-bold">
							<a href="login" class="text-white">Login</a>
							</h6>
						</div>

						</div>
					</section>

					<hr class="my-5" />
					<section class="mb-5">
						<div class="row d-flex justify-content-center">
						<div class="col-lg-8">
							<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt
							distinctio earum repellat quaerat voluptatibus placeat nam,
							commodi optio pariatur est quia magnam eum harum corrupti
							dicta, aliquam sequi voluptate quas.
							</p>
						</div>
						</div>
					</section>
			</div>

		</footer>
		</div>



@endsection
