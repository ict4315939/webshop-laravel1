@extends('layoutadmin')
@section('title', 'categoriebeher')

@section('content')
 
        <div class="ml-4 text-center text-sm text-gray-500 dark:text-gray-400 sm:text-right sm:ml-5">
            <h2>Categorieën</h2><br>
            <a href="{{ url('/categorie/create') }}" class="toevoeg">Voeg een categorie toe</a><br>

        </div>






 <div class="card mx-auto" style="width: 70rem;">

    <table class="table table-bordered table-responsive-md">
        @if(session('message'))
            <div class="text-center">
                <p class="alert alert-secondary">{{ session('message') }}</p>
            </div>
        @endif
       @if($categorie->isEmpty())
        
        <p class="alert ">Geen Categorieën gevonden</p>

       @else
       <thead>
        <tr>
        <th scope="col">Categorieën</th>
        <th scope="col">Verwijderen</th>
        <th scope="col">Aanpassen</th>
        </tr>
        </thead>
        <tbody>

          @foreach($categorie as $item)
              <tr>
              <td>{{ $item->category }}</td>
              <td>            
                  <form method="POST" action="{{ url('/deletecategorie' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                      {{ csrf_field() }}
                      <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Weet je zeker dat je dit categorie wilt verwijderen?')" title="Delete categorie"> Delete</button>
                  </form></td>
                  <td>
                  <a href="{{ url('/categorie/' . $item->id . '/edit') }}" title="Edit Student"><button class="btn btn-primary btn-sm"> Edit</button></a>
                  </td>
              </tr>
            @endforeach
       @endif
       
      </tbody>
    </table>
 </div>

@endsection