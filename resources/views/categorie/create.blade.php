@extends('layoutadmin')
@section('title', 'create')

@section('content')
<div class="card">
 <div class="card-header">Categorieën</div>
  <div class="card-body">
      
      <form action="{{ url('categorie') }}" method="post">
        {!! csrf_field() !!}
        <label>Categorie</label></br>
        <input type="text" name="category" id="name" class="form-control"></br>

        <input type="submit" value="Save" class="btn btn-success"></br>
    </form>
   
  </div>
</div>


@endsection