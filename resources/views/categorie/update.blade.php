@extends('layoutadmin')
@section('title', 'update')

@section('content')


<div class="card">
  <div class="card-header">Edit categorie</div>
  <div class="card-body">
      
      <form action="{{ url('categorie/' .$categorie->id) }}" method="post">
        {!! csrf_field() !!}
        @method("PATCH")
        <input type="hidden" name="id" value="{{$categorie->id}}" id="id" />
        <label>categorie</label></br>
        <input type="text" name="category" value="{{$categorie->category}}" class="form-control"></br>
        <input type="submit" value="Update" class="btn btn-success"></br>
    </form>
   
  </div>
</div>

@endsection