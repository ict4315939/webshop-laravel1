<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Cart;
use App\Models\Orderitems;
use App\Models\Product;

use App\Models\orders;

class OrderController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $userinfo = User::where('id', $user->id)->get();
        $cartinfo = Cart::where('user_id', $user->id)->get();

        if (count($cartinfo) > 0){
          return view('bestellen')->with('user', $userinfo)->with('cartinfo',$cartinfo); 


        }else{
            return redirect('winkelwagen')->with('error', 'Voeg eerst iets toe aan de winkelwagen!');
            
        }





    } 
    public function orderguest(){
        if(session('cart')){
            if(auth()->check()){
                $user = auth()->user();
    
    
                $cart = Cart::where('user_id', $user->id)->get();
        
            }else{
                $cart = Cart::all();
    
            }        
            return view('bestelguest')->with('cart', $cart);


        }else{
            return redirect('winkelwagen')->with('error', 'Voeg eerst iets toe aan de winkelwagen!');

        }




    }
    public function guestorder( Request $request){
       // $user = auth()->user();



        $order = new orders();
     //   $order->user_id = $user->id;
        $order->voornaam = $request->voornaam;
        $order->achternaam = $request->achternaam;
        $order->woonplaats = $request->woonplaats;
        $order->straat = $request->straat;
        $order->huisnummer = $request->huisnummer;
        $order->postcode = $request->postcode;
        $order->email = $request->email;
        $order->totalprice = $request->totalprice;
        $order->save();

        $order_id = $order->order_id;

        $cart = session()->get('cart', []);



        foreach ($cart as $product_id => $cartItem) {
            $orderitem = new Orderitems();
            $orderitem->order_id = $order_id;
            $orderitem->product_id = $product_id;
            $orderitem->Pamount = $cartItem['Pstock'];
            $orderitem->price = $cartItem['Pprice'];
            $orderitem->save();

            $product = Product::findOrfail($product_id);
            $product->Pstock -= $cartItem['Pstock'];
            $product->save();


            unset($cart[$product_id]);

        }
        session()->put('cart', $cart);
    
        // if (isset($cart[$product_id])) {
        //     unset($cart[$product_id]);
        //     session()->put('cart', $cart);
        // }
        return redirect('suplementen')->with('success', 'bedankt voor je besteling');
    }
    public function order( Request $request){
        
         $user = auth()->user();
         $cartItems = Cart::where('user_id',$user->id)->get();
 
 
 
         $order = new orders();
         $order->user_id = $user->id;
         $order->voornaam = $request->voornaam;
         $order->achternaam = $request->achternaam;
         $order->woonplaats = $request->woonplaats;
         $order->straat = $request->straat;
         $order->huisnummer = $request->huisnummer;
         $order->postcode = $request->postcode;
         $order->email = $request->email;
         $order->totalprice = $request->totalprice;
         $order->save();
 
         $order_id = $order->order_id;

         foreach ($cartItems as $cartItem ) {
             $orderitem = new Orderitems();
             $orderitem->order_id = $order_id;
             $orderitem->product_id = $cartItem->product_id;
             $orderitem->Pamount = $cartItem->amount;
             $orderitem->price = $request->price;
             $orderitem->save();
 
             $product = Product::findOrfail($cartItem->product_id);
             $product->Pstock -= $request->Pamount;
             $product->save();
             $cartItem->delete();


         }
         return redirect('suplementen')->with('success', 'bedankt voor je besteling');
     }

}
