<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\orders;
use App\Models\User;
use App\Models\Orderitems;



class OrdersController extends Controller
{
   public function index(){
    $user = auth()->user();
    $userOrder = orders::where('user_id', $user->id)->get();

        $orderitems = Orderitems::all();


    return view('bestellingen')->with('orders', $userOrder)->with('orderitems', $orderitems);    ;
   }



   public function ordersoverview(){
    $orderinfo = orders::all();
    $orderitems = Orderitems::all();
    return view('bestellingenoverzicht')->with('orderinfo', $orderinfo)->with('orderitems', $orderitems);

   }
}
