<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categorie = Category::all();
        return view('categorie.categoriebeher')->with('categorie',$categorie); 
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('categorie.create');

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Category::create($input);
        return redirect('categorie')->with('message', 'Categorie is toegevoegd!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {



  
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $categorie = Category::find($id);
        return view('categorie.update')->with('categorie', $categorie);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $categorie = Category::find($id);
        $input = $request->all();
        $categorie->update($input);
        return redirect('categorie')->with('message', 'Categorie is aangepast!'); 
    }

    /**
     * Remove the specified resource from storage.
     */


    public function deletecategorie(Request $request, $id){
        $categorie = Category::where('id', $id)->first();
        $categorie->category = 'Niet Active';
        $categorie->save(); 
        return redirect('categorie')->with('message', 'categorie is verwijderd!'); 

    }
}
