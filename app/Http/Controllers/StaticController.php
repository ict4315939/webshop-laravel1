<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use App\Models\Category;
use App\Models\Cart;



use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Stmt\Return_;

class StaticController extends Controller
{
    public function index(){
 
        return view('welcome');
    }
    public function welcome(){
        return view('welcome');
    }
    public function login(){
        return view('login');
    }
    public function categoriebeher(){
        return view('categorie');
    }
    // public function layout() {
    //     // Get the currently authenticated user's user_id
    //     $user_id = auth()->user()->id;
    
    //     // Query the Cart table to count items for the current user
    //     $cartItemCount = Cart::where('user_id', $user_id)->count();
    
    //     // Pass the cartItemCount to the view
    //     return view('layout')->with('cart', $cartItemCount);
    // }
    public function productenbeher(){
        return view('producten');
    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();

        $request->session()->regenerateToken();
    
        return redirect('/');
    }
    public function loginPost(Request $request){
            $credetials = [
                'email' => $request->email,
                'password' => $request->password,
            ];
            if(Auth::attempt($credetials)){
                
                $user = Auth::user();

                if ($user->role_id === 2) {
                    return redirect('categorie');
                } elseif ($user->role_id === 1) {
                    return redirect('/welcome');
                }
            }
            return back()->with('error', 'email of wachtwoord klopt niet');

    }
    public function registreren(){
        return view('registreren');
    }
    public function registrerenPost(Request $request){

        $rules = [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => ['required','string','min:8','regex:/[0-9]/','regex:/[@$!%*#?&]/',],
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()
            ->withErrors($validator)
            ->withInput();
        }

        $user = new User();

        $user->voornaam = $request->voornaam;
        $user->achternaam = $request->achternaam;
        $user->woonplaats = $request->woonplaats;
        $user->straat = $request->straat;
        $user->huisnummer = $request->huisnummer;
        $user->postcode = $request->postcode;
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return back()->with('success', 'Er is een account aangemaakt');
    }




}



