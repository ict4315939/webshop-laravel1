<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;


class WinkelwagenController extends Controller
{
    public function index(){

        if(auth()->check()){
            $user = auth()->user();


            $cart = Cart::where('user_id', $user->id)->get();
    
        }else{
            $cart = Cart::all();

        }

        

        return view('winkelwagen')->with('cart', $cart);


    }
    public function deleteProduct(Request $request)
    {
        if($request->product_id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->product_id])) {
                unset($cart[$request->product_id]);
                session()->put('cart', $cart);
            }
            return redirect('winkelwagen')->with('message', 'product is verwijderd!'); 
        }
        return redirect('winkelwagen')->with('message', 'is niet gevonden!',404); 

    }
    public function updateproduct(Request $request, $product_id)
    {
        $newQuantity = $request->input('Pstock');
    
        // Retrieve the current cart from the session
        $cart = session()->get('cart', []);
    
        if (isset($cart[$product_id])) {
            // Update the quantity for the specified product
            $cart[$product_id]['Pstock'] = $newQuantity;
            
            // Save the updated cart back to the session
            session()->put('cart', $cart);
        }
    
        return redirect()->back()->with('success', 'Quantity updated successfully');
    }
    public function updatecartproduct(Request $request, $cart_id)
    {
        $cartProduct = Cart::where('cart_id',$cart_id)->first( );
        $cartProduct->amount = $request->Pstock;
        $cartProduct->save();
        return redirect()->back()->with('success', 'Quantity updated successfully');
    }


    
    public function destroy(string $cart_id)
    {
        Cart::destroy($cart_id);
        return redirect('winkelwagen')->with('message', 'product is verwijderd!'); 
    }
}
