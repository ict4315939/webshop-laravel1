<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;



class ProductenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $producten = Product::where('available', true)->get();

        return view('producten.index')->with('producten',$producten); 
    } 

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categorie = Category::all();

        return view('producten.create')->with('categorie',$categorie); 

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $imageName = time().'.'.$request->Pimage->extension();
        $request->Pimage->move(public_path('products'), $imageName);
        $product = new Product();
        $product->Pimage = $imageName;
        $product->Pname = $request->Pname;
        $product->Ptaste = $request->Ptaste;
        $product->Pdescription = $request->Pdescription;
        $product->Pstock = $request->Pstock;
        $product->Pprice = $request->Pprice;
        $product->Pweight = $request->Pweight;
        $product->category_id = $request->category_id;

        $product->save();
        return redirect('producten')->with('message', 'product is toegevoegd!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
        $categorie = Category::all();
        $producten = Product::find($id);
        
        return view('producten.aanpassen')->with('producten', $producten)->with('categorie', $categorie);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product = Product::where('product_id',$id)->first( );
        if(isset($request->Pimage)){
            $imageName = time().'.'.$request->Pimage->extension();
            $request->Pimage->move(public_path('products'), $imageName);
            $product->Pimage = $imageName;
        }
        $product->Pname = $request->Pname;
        $product->Ptaste = $request->Ptaste;
        $product->Pdescription = $request->Pdescription;
        $product->Pstock = $request->Pstock;
        $product->Pprice = $request->Pprice;
        $product->Pweight = $request->Pweight;
        $product->category_id = $request->category_id;

        $product->save();
        //$input = $request->all();
       // $producten->update($input);
        return redirect('producten')->with('message', 'product is aangepast!'); 
    }

    /**
     * Remove the specified resource from storage.
     */
    public function updateproductitem(Request $request, $product_id)
    {
        $deletedProduct = Product::where('product_id',$product_id)->first( );
        $deletedProduct->available = false; 
        $deletedProduct->Pstock = 0;
        $deletedProduct->save();
        return redirect('producten')->with('message', 'product is verwijderd!'); 
    }
}
