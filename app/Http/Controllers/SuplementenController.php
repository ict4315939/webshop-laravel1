<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Category;
use App\Models\Cart;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SuplementenController extends Controller
{
    public function index(){
        $producten = Product::where('available', true)->get();
        $categorie = Category::all();

        
        return view('suplementen')->with('producten', $producten)->with('categorie', $categorie);
    }
    public function filter(Request $request)
    {
        $categoryId = $request->category;
        if($categoryId == 'AlleProducten'){
            $producten = Product::all();
            $categorie = Category::all();
    
            
            return view('suplementen')->with('producten', $producten)->with('categorie', $categorie);

        }else{

        $producten = Product::where('category_id', $categoryId)->get();
        $categorie = Category::all(); 
        return view('suplementen', compact('producten', 'categorie'));
        }

    }
    public function addtocart($product_id){
         //zonder database
          $product = Product::FindOrFail($product_id);
          $cart = session()->get('cart', []);
          if(isset($cart[$product_id])){
            $cart[$product_id]['Pstock']++;

          }else{
            $cart[$product_id] = [
              //  "product_id" => $product->product_id,
               "Pname" => $product->Pname,
                "available" => $product->available,
                "Pprice" => $product->Pprice,
                "Pstock" => 1

            ];
          }
          session()->put('cart', $cart);
          return redirect()->back()->with('success','product is toegevoegd aan de winkelwagen');

    }


    public function add_to_cart(Request $request, $product)
    {
        $user = auth()->user();
    
        $product = Product::find($product);
    
        if (!$product) {
            abort(404); 
        }
      $existingItem = Cart::where('user_id', $user->id)->where('product_id', $product->product_id)->first();
      if($existingItem){
        $existingItem->increment('amount', 1);


        return redirect()->back()->with('success', 'product is toegevoegd aan de winkelwagen!');

      }else{
        Cart::create([
            'user_id' => $user->id,
            'product_id' => $product->product_id,
            'amount' => 1, 
        ]);
    
        return redirect()->back()->with('success', 'product is toegevoegd aan de winkelwagen!');
      }
    }
     



    //         ];
    //     }
    //     session()->put('cart', $cart);
    //     return view('winkelwagen', ['cart' => $cart]);
    // }
    // public function removefromcart($product_id) {
    //     $cart = session()->get('cart', []);
    
    //     // Check if the product exists in the cart
    //     if (isset($cart[$product_id])) {
    //         // Remove the product from the cart
    //         unset($cart[$product_id]);
    //         session()->put('cart', $cart);
    //     }
    
    //     return view('winkelwagen', ['cart' => $cart]);
    // }
}
