<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'shoppingcart';
    protected $primaryKey = 'cart_id';
    protected $fillable = ['user_id','product_id', 'amount' ];
    public $timestamps = true; 
    use HasFactory;

    public function productcart(){
        return $this->belongsTo(Product::class, 'product_id');

    }


}
