<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{

    protected $table = 'products';
    protected $primaryKey = 'product_id';
    protected $fillable = ['Pname','Ptaste', 'Pdescription', 'Pstock', 'Pprice', 'Pweight', 'Pimage', 'category_id', 'available' ];
    public $timestamps = true; 
    use HasFactory;

    public function category(){
        return $this->belongsTo(Category::class); 
    }
    public function cart(){
        return $this->hasMany(Cart::class);
    }
    public function orderItems(){
        return $this->hasMany(Orderitems::class);
    }

}
