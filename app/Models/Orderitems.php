<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderitems extends Model
{
    protected $table = 'orderitems';
    protected $primaryKey = 'order_id';
    protected $fillable = ['order_id','product_id', 'Pamount', 'price' ];
    public $timestamps = false;

    use HasFactory;
    public function orderproduct(){
        return $this->belongsTo(Product::class,'product_id'); 
    }
    public function orders(){
        return $this->hasMany(orders::class);
    }
}
