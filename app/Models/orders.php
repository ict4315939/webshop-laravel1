<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class orders extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'order_id';
    protected $fillable = ['user_id','voornaam', 'achternaam', 'woonplaats', 'straat', 'huisnummer', 'postcode', 'email', 'totalprice', 'paid' ];
    public $timestamps = true; 
    use HasFactory;

    public function orderitems(){
        return $this->belongsTo(Orderitems::class, 'order_id');

    }
}
