<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\ProductenController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaticController;
use App\Http\Controllers\SuplementenController;
use App\Http\Controllers\WinkelwagenController;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


    Route::get('/',[StaticController::class, 'index']);
    Route::get('/suplementen',[SuplementenController::class, 'index']);
    Route::get('/filter',[SuplementenController::class, 'filter']);
    Route::get('toevoegen_aan_winkelwagen/{product_id}', [SuplementenController::class, 'addtocart'])->name('addtocart');
    Route::get('product/{product_id}', [SuplementenController::class, 'add_to_cart'])->name('add_to_cart');
    Route::post('update-cart-product/{product_id}', [WinkelwagenController::class, 'updateproduct'])->name('update-cart-product');
    Route::post('updatecartitem/{cart_id}', [WinkelwagenController::class, 'updatecartproduct'])->name('updatecartitem');


    Route::delete('/delete-cart-product/{product_id}', [WinkelwagenController::class, 'deleteProduct'])->name('delete-cart-product');
    Route::delete('/deletecartproduct/{cart_id}', [WinkelwagenController::class, 'destroy'])->name('deletecartproduct');



    Route::get('/winkelwagen',[WinkelwagenController::class, 'index']);
    Route::post('bestellen',[OrderController::class, 'guestorder'])->name('bestellen');
    Route::get('/bestelbezoeker',[OrderController::class, 'orderguest']);



    Route::get('/welcome',[HomeController::class, 'welcome']);
    Route::get('/login',[StaticController::class, 'login'])->name('login');
    Route::post('/login',[StaticController::class, 'loginPost'])->name('login');

    Route::get('/registreren',[StaticController::class, 'registreren'])->name('registreren');
    Route::post('/registreren',[StaticController::class, 'registrerenPost'])->name('registreren');


    Route::get('/bestellen',[OrderController::class, 'index']);
    Route::post('updateproductitem/{product_id}', [ProductenController::class, 'updateproductitem'])->name('updateproductitem');
    Route::post('ordercart',[OrderController::class, 'order'])->name('ordercart');
    Route::get('bestellingen', [OrdersController::class, 'index']);
    Route::delete('/logou',[HomeController::class, 'logout'])->name('logou');


Route::group(['middleware' => 'admin'], function() {

    Route::resource('categorie', CategoryController::class);
    Route::resource('producten', ProductenController::class);
    Route::get('bestellingenoverzicht', [OrdersController::class, 'ordersoverview']);
    Route::post('updateproductitem/{product_id}', [ProductenController::class, 'updateproductitem'])->name('updateproductitem');
    Route::post('deletecategorie/{id}', [CategoryController::class, 'deletecategorie'])->name('deletecategorie');

    
    Route::delete('/logout',[StaticController::class, 'logout'])->name('logout');

});