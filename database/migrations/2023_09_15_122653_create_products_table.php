<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id('product_id');
            $table->string('Pname');
            $table->string('Ptaste');
            $table->longText('Pdescription');
            $table->integer('Pstock');
            $table->decimal('Pprice', 8, 2);
            $table->string('Pweight');
            $table->string('Pimage');
            $table->boolean('available')->default(true);

            $table->foreignId('category_id')->references('id')->on('category');


            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
