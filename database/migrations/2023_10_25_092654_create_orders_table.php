<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\ForeignIdColumnDefinition;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id('order_id');
            $table->foreignId('user_id')->nullable()->references('id')->on('users');
            $table->string('voornaam');
            $table->string('achternaam');
            $table->string('woonplaats');
            $table->string('straat');
            $table->string('huisnummer');
            $table->string('postcode');
            $table->string('email');
            $table->decimal('totalprice', 8, 2);

            $table->boolean('paid')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
